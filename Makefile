all: main.o

main.o: main.cc
	g++ -c -std=c++11 main.cc
	g++  main.o -o project1

clean:
	rm -rf *o  project1

run1:
	time -p ./project1 testcases/testcase1.txt test1.txt

test1:
	vimdiff test1.txt testcases/result1.txt

run2:
	time -p ./project1 testcases/testcase2.txt test2.txt
test2:
	vimdiff test2.txt testcases/result2.txt

run3:
	time -p ./project1 testcases/testcase3.txt test3.txt
test3:
	vimdiff test3.txt testcases/result3.txt

run4:
	time -p ./project1 testcases/testcase4.txt test4.txt
test4:
	vimdiff test4.txt testcases/result4.txt


