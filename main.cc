#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <queue>
#include <iomanip>
#include <utility>
#include <vector>


using namespace std;

//struct to hold the points
struct ListNode {
	int id ;
	double xLoc ;
	double yLoc ;
	double relDist ;
};

//struct to use it in priority queue
struct Tuple {
	double relDist ;
	int id ;
};
class compareDistance {// this class is used by priority queue for comparison
public :
	bool operator()(Tuple& l1, Tuple& l2) {
		if(l2.relDist < l1.relDist) return true ;// give back minimum relative distance
		return false ;
	}
};
//Function declaration
double calcDist(ListNode source,ListNode target) ;


int main(int argc, char *argv[]){
	/* obtain file names */
	string inFile = argv[1];
	string outFile = argv[2];
	// opening resources
	ifstream inFileStream ;
	ofstream outFileStream ;
	//open input file
	inFileStream.open(inFile.c_str());
	// Check to see if file exists
	if(inFileStream.fail()) {
		cout << "File named " << inFile << " could not be opened.\n" ;
		cout << "File does not exist." << endl ;

		exit(1);
	}
	//check to see if N and k are valid
	int nNumber {0};
	int kNumber {0} ;
	inFileStream >> nNumber ;
	inFileStream >> kNumber ;
	if(!(nNumber > kNumber && nNumber > 0 && kNumber >= 0)){
		cerr << "Node number and neighbour number are not valid" << endl ;
		exit(1);
	}
    //creating array for the points
	vector <ListNode> numList(nNumber);
	// fill up the array
	for(int index = 0 ; index < nNumber ; index++) {
		numList[index].id = index + 1 ;
		inFileStream >> numList[index].xLoc ;
		inFileStream >> numList[index].yLoc ;
	}
	inFileStream.close();
	// Create the priority queue
	priority_queue<Tuple, vector<Tuple>, compareDistance> distQue ;
	ListNode tempSource ;// focused point
	ListNode tempTarget ;// checking this to obtain relative distance
	double relDistTemp = 0.0 ;
	outFileStream.open(outFile.c_str());// open output file (and create)
	Tuple transition ;// creating this for easy file writing
    //starting to collect relative distances
	for(int index = 0 ; index < nNumber ; index++){
		tempSource = numList[index] ;

		for(int index2 = 0 ; index2 < nNumber ; index2++) {
			tempTarget = numList[index2]  ;
			relDistTemp = calcDist(tempSource,tempTarget) ;
			Tuple temp ;
			temp.relDist = relDistTemp ;
			temp.id = index2 +1 ;
			distQue.push(temp);// into the queue

		}

		transition = distQue.top();// take out the actual point first
		outFileStream << transition.id ;// write its id to the output file
		distQue.pop();//get rid of that entry
		for(int i = 0 ; i < kNumber ; i++){// nearest neighbours to the output file
			transition = distQue.top();
			outFileStream << " " ;
			outFileStream << transition.id ;
			distQue.pop();
		}
		outFileStream << endl ;
		// empty the bucket
        distQue = priority_queue<Tuple,vector<Tuple>,compareDistance>();
	}
	outFileStream.close();

	return 0;
}
// relative distance calculation
double calcDist(ListNode source,ListNode target) {
	return (pow(abs((target.xLoc - source.xLoc)),2) + pow(abs((target.yLoc - source.yLoc)),2));
}


